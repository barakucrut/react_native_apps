/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView
} from 'react-native';

import ScrollableTabView from 'react-native-scrollable-tab-view';

export default class Main extends Component {
  render() {
    return (
      <View style = {styles.container}>
        <View style = {styles.headerWrapper}>
          <View style = {styles.topHeader}>
              <View style = {styles.topHeaderLeft}>
                <View>
                    <Text style = {styles.logo}>MINUTES</Text>
                </View>
                <View>
                    <Text style = {styles.subLogo}>Provider</Text>
                </View>
              </View>
              <View style = {styles.topHeaderRight}>
                <TouchableOpacity style= {styles.changeCalendar}>
                 <Image 
                    style={{height:23,width:23}}
                    source={require('../img/ic_tab_my_appointment.png')} 
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                 <Image 
                    style={{height:25,width:25}}
                    source={require('../img/ic_menu_settings.png')} 
                  />
                </TouchableOpacity>                
              </View>
          </View>          
        </View>        
        <View style = {styles.bodyWrapper}>          
          <ScrollableTabView
            tabBarBackgroundColor = '#2882c0'
            tabBarActiveTextColor = 'white'
            tabBarInactiveTextColor = 'white'
            tabBarTextStyle = {{fontFamily: 'myriad', fontSize: 15, fontWeight: 'bold'}}
            tabBarUnderlineStyle = {{backgroundColor:'white',height:2}}
            prerenderingSiblingsNumber={Infinity}
          >
              
              <ScrollView 
                tabLabel="Checkin" 
                style={styles.tabView}
                scrollEnabled={true}
                showsVerticalScrollIndicator={true}
                keyboardShouldPersistTaps='always'
                keyboardDismissMode='on-drag'
              >
                <CardAppointment type = 'booking'/>
              </ScrollView>
              <ScrollView tabLabel="Service" style={styles.tabView}>
                <CardAppointment type = 'booking'/>
              </ScrollView>
              <ScrollView tabLabel="Done" style={styles.tabView}>
                <CardAppointment type = 'booking'/>
              </ScrollView>
          </ScrollableTabView>          
        </View>      
      </View>
    )
  }
}



class CardAppointment extends Component {
  render() {
    var styling = null;
    switch(this.props.type){
      case 'booking' :
        styling = styles.bookingCard;
      default:
        styling = styles.bookingCard;
    }
    return (
      <View>
        <View style={styles.card}>
          <View style={styles.cardTop}>
            <View style={styles.cardAvatar}>
              <Image 
                style={{height:50,width:50}}
                source={require('../img/ic_avatar.png')} 
              />
            </View>
            <View style = {styles.cardInfo}>
              <Text style = {styles.cardName}>Ferli Juli Herlambang</Text>
              <Text style = {styles.cardTime}>10:20 - 10:50</Text>
              <Text style = {styles.cardService}>Highlight Full Hair</Text>
            </View>
          </View>
          <View style = {styles.cardAction}>
            <View style = {[styles.cardActionWrapperLeft]}>
              <TouchableOpacity style = {[styles.touchAction,styles.touchActionLeft]}>
                  <Text style= {styles.buttonCard}>More Info</Text>  
              </TouchableOpacity>
            </View>
            <View style = {{width:1,backgroundColor:'#dedede'}}></View>
            <View style= {[styles.cardActionWrapper, styles.borderRightStyle]}>
              <TouchableOpacity style = {styles.touchAction}>
                  <Text style= {styles.buttonCard}>Checkin Now</Text>  
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.cardTop}>
            <View style={styles.cardAvatar}>
              <Image 
                style={{height:50,width:50}}
                source={require('../img/ic_avatar.png')} 
              />
            </View>
            <View style = {styles.cardInfo}>
              <Text style = {styles.cardName}>Ferli Juli Herlambang</Text>
              <Text style = {styles.cardTime}>10:20 - 10:50</Text>
              <Text style = {styles.cardService}>Highlight Full Hair</Text>
            </View>
          </View>
          <View style = {styles.cardAction}>
            <View style = {[styles.cardActionWrapperLeft]}>
              <TouchableOpacity style = {[styles.touchAction,styles.touchActionLeft]}>
                  <Text style= {styles.buttonCard}>More Info</Text>  
              </TouchableOpacity>
            </View>
            <View style = {{width:1,backgroundColor:'#dedede'}}></View>
            <View style= {[styles.cardActionWrapper, styles.borderRightStyle]}>
              <TouchableOpacity style = {styles.touchAction}>
                  <Text style= {styles.buttonCard}>Checkin Now</Text>  
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.cardTop}>
            <View style={styles.cardAvatar}>
              <Image 
                style={{height:50,width:50}}
                source={require('../img/ic_avatar.png')} 
              />
            </View>
            <View style = {styles.cardInfo}>
              <Text style = {styles.cardName}>Ferli Juli Herlambang</Text>
              <Text style = {styles.cardTime}>10:20 - 10:50</Text>
              <Text style = {styles.cardService}>Highlight Full Hair</Text>
            </View>
          </View>
          <View style = {styles.cardAction}>
            <View style = {[styles.cardActionWrapperLeft]}>
              <TouchableOpacity style = {[styles.touchAction,styles.touchActionLeft]}>
                  <Text style= {styles.buttonCard}>More Info</Text>  
              </TouchableOpacity>
            </View>
            <View style = {{width:1,backgroundColor:'#dedede'}}></View>
            <View style= {[styles.cardActionWrapper, styles.borderRightStyle]}>
              <TouchableOpacity style = {styles.touchAction}>
                  <Text style= {styles.buttonCard}>Checkin Now</Text>  
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.cardTop}>
            <View style={styles.cardAvatar}>
              <Image 
                style={{height:50,width:50}}
                source={require('../img/ic_avatar.png')} 
              />
            </View>
            <View style = {styles.cardInfo}>
              <Text style = {styles.cardName}>Ferli Juli Herlambang</Text>
              <Text style = {styles.cardTime}>10:20 - 10:50</Text>
              <Text style = {styles.cardService}>Highlight Full Hair</Text>
            </View>
          </View>
          <View style = {styles.cardAction}>
            <View style = {[styles.cardActionWrapperLeft]}>
              <TouchableOpacity style = {[styles.touchAction,styles.touchActionLeft]}>
                  <Text style= {styles.buttonCard}>More Info</Text>  
              </TouchableOpacity>
            </View>
            <View style = {{width:1,backgroundColor:'#dedede'}}></View>
            <View style= {[styles.cardActionWrapper, styles.borderRightStyle]}>
              <TouchableOpacity style = {styles.touchAction}>
                  <Text style= {styles.buttonCard}>Checkin Now</Text>  
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.cardTop}>
            <View style={styles.cardAvatar}>
              <Image 
                style={{height:50,width:50}}
                source={require('../img/ic_avatar.png')} 
              />
            </View>
            <View style = {styles.cardInfo}>
              <Text style = {styles.cardName}>Ferli Juli Herlambang</Text>
              <Text style = {styles.cardTime}>10:20 - 10:50</Text>
              <Text style = {styles.cardService}>Highlight Full Hair</Text>
            </View>
          </View>
          <View style = {styles.cardAction}>
            <View style = {[styles.cardActionWrapperLeft]}>
              <TouchableOpacity style = {[styles.touchAction,styles.touchActionLeft]}>
                  <Text style= {styles.buttonCard}>More Info</Text>  
              </TouchableOpacity>
            </View>
            <View style = {{width:1,backgroundColor:'#dedede'}}></View>
            <View style= {[styles.cardActionWrapper, styles.borderRightStyle]}>
              <TouchableOpacity style = {styles.touchAction}>
                  <Text style= {styles.buttonCard}>Checkin Now</Text>  
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.cardTop}>
            <View style={styles.cardAvatar}>
              <Image 
                style={{height:50,width:50}}
                source={require('../img/ic_avatar.png')} 
              />
            </View>
            <View style = {styles.cardInfo}>
              <Text style = {styles.cardName}>Ferli Juli Herlambang</Text>
              <Text style = {styles.cardTime}>10:20 - 10:50</Text>
              <Text style = {styles.cardService}>Highlight Full Hair</Text>
            </View>
          </View>
          <View style = {styles.cardAction}>
            <View style = {[styles.cardActionWrapperLeft]}>
              <TouchableOpacity style = {[styles.touchAction,styles.touchActionLeft]}>
                  <Text style= {styles.buttonCard}>More Info</Text>  
              </TouchableOpacity>
            </View>
            <View style = {{width:1,backgroundColor:'#dedede'}}></View>
            <View style= {[styles.cardActionWrapper, styles.borderRightStyle]}>
              <TouchableOpacity style = {styles.touchAction}>
                  <Text style= {styles.buttonCard}>Checkin Now</Text>  
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.cardTop}>
            <View style={styles.cardAvatar}>
              <Image 
                style={{height:50,width:50}}
                source={require('../img/ic_avatar.png')} 
              />
            </View>
            <View style = {styles.cardInfo}>
              <Text style = {styles.cardName}>Ferli Juli Herlambang</Text>
              <Text style = {styles.cardTime}>10:20 - 10:50</Text>
              <Text style = {styles.cardService}>Highlight Full Hair</Text>
            </View>
          </View>
          <View style = {styles.cardAction}>
            <View style = {[styles.cardActionWrapperLeft]}>
              <TouchableOpacity style = {[styles.touchAction,styles.touchActionLeft]}>
                  <Text style= {styles.buttonCard}>More Info</Text>  
              </TouchableOpacity>
            </View>
            <View style = {{width:1,backgroundColor:'#dedede'}}></View>
            <View style= {[styles.cardActionWrapper, styles.borderRightStyle]}>
              <TouchableOpacity style = {styles.touchAction}>
                  <Text style= {styles.buttonCard}>Checkin Now</Text>  
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.cardTop}>
            <View style={styles.cardAvatar}>
              <Image 
                style={{height:50,width:50}}
                source={require('../img/ic_avatar.png')} 
              />
            </View>
            <View style = {styles.cardInfo}>
              <Text style = {styles.cardName}>Ferli Juli Herlambang</Text>
              <Text style = {styles.cardTime}>10:20 - 10:50</Text>
              <Text style = {styles.cardService}>Highlight Full Hair</Text>
            </View>
          </View>
          <View style = {styles.cardAction}>
            <View style = {[styles.cardActionWrapperLeft]}>
              <TouchableOpacity style = {[styles.touchAction,styles.touchActionLeft]}>
                  <Text style= {styles.buttonCard}>More Info</Text>  
              </TouchableOpacity>
            </View>
            <View style = {{width:1,backgroundColor:'#dedede'}}></View>
            <View style= {[styles.cardActionWrapper, styles.borderRightStyle]}>
              <TouchableOpacity style = {styles.touchAction}>
                  <Text style= {styles.buttonCard}>Checkin Now</Text>  
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.cardTop}>
            <View style={styles.cardAvatar}>
              <Image 
                style={{height:50,width:50}}
                source={require('../img/ic_avatar.png')} 
              />
            </View>
            <View style = {styles.cardInfo}>
              <Text style = {styles.cardName}>Ferli Juli Herlambang</Text>
              <Text style = {styles.cardTime}>10:20 - 10:50</Text>
              <Text style = {styles.cardService}>Highlight Full Hair</Text>
            </View>
          </View>
          <View style = {styles.cardAction}>
            <View style = {[styles.cardActionWrapperLeft]}>
              <TouchableOpacity style = {[styles.touchAction,styles.touchActionLeft]}>
                  <Text style= {styles.buttonCard}>More Info</Text>  
              </TouchableOpacity>
            </View>
            <View style = {{width:1,backgroundColor:'#dedede'}}></View>
            <View style= {[styles.cardActionWrapper, styles.borderRightStyle]}>
              <TouchableOpacity style = {styles.touchAction}>
                  <Text style= {styles.buttonCard}>Checkin Now</Text>  
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.cardTop}>
            <View style={styles.cardAvatar}>
              <Image 
                style={{height:50,width:50}}
                source={require('../img/ic_avatar.png')} 
              />
            </View>
            <View style = {styles.cardInfo}>
              <Text style = {styles.cardName}>Ferli Juli Herlambang</Text>
              <Text style = {styles.cardTime}>10:20 - 10:50</Text>
              <Text style = {styles.cardService}>Highlight Full Hair</Text>
            </View>
          </View>
          <View style = {styles.cardAction}>
            <View style = {[styles.cardActionWrapperLeft]}>
              <TouchableOpacity style = {[styles.touchAction,styles.touchActionLeft]}>
                  <Text style= {styles.buttonCard}>More Info</Text>  
              </TouchableOpacity>
            </View>
            <View style = {{width:1,backgroundColor:'#dedede'}}></View>
            <View style= {[styles.cardActionWrapper, styles.borderRightStyle]}>
              <TouchableOpacity style = {styles.touchAction}>
                  <Text style= {styles.buttonCard}>Checkin Now</Text>  
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({  
  touchAction:{
    padding:15,
    flex:1
  },
  cardActionWrapperLeft:{    
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    borderBottomLeftRadius:5,
    backgroundColor:'#f0efef'
  },
  borderRightStyle:{    
    borderBottomRightRadius :5
  },
  tabView: {
    flex: 1,
    padding: 10,
    backgroundColor:'#efefef'
  },
  card: {
    backgroundColor: '#fff',    
    margin: 5,        
    shadowColor: 'black',
    shadowOffset: { width: 100, height: 100, },
    shadowOpacity: 0.8,
    shadowRadius: 10,
    borderRadius:5,
    borderWidth:1,
    borderColor:'#dedede',
    borderWidth:1
  },
  cardAvatar:{
    padding:10,
    paddingRight:5
  },

  cardTop:{
    flexDirection:'row'
  },
  cardName:{
    fontFamily:'myriad',
    fontWeight:'bold' 
  },
  cardTime:{
    fontSize:25,
    fontFamily:'roboto_light',
    color:'#b5b5b5',
    paddingTop:5,
    paddingBottom:5
  },
  cardService:{
    fontSize:15,
    fontFamily:'roboto_light',
    color:'#b5b5b5',    
    paddingBottom:5
  },
  cardAction:{
    flexDirection:'row',
    borderTopWidth:1,
    borderTopColor:'#dedede',
  },
  cardActionWrapper:{    
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#f0efef'
  },
  buttonCard:{

  },
  cardInfo:{
    padding:10
  },
  changeCalendar:{
    marginRight:15
  },
  logo:{
      fontFamily:'museo900',
      fontSize:25,
      color:'white'
  },
  subLogo:{
      fontFamily:'myriad',
      fontSize:15,
      color:'white',
      paddingLeft:2,
      paddingTop:1      
  },
  topHeaderRight:{
      flex:1,
      flexDirection:'row',
      justifyContent:'flex-end',
      paddingTop:3
  },
  topHeaderLeft:{
      flex:3,
      justifyContent: 'flex-start',
      flexDirection:'row'
  },
  topHeader:{
      padding:10,
      flexDirection:'row'
  },
  container:{
      flex:1
  },
  headerWrapper:{
    height:55,
    backgroundColor:'#2882c0',    
  },
  bodyWrapper:{
    flex:1
  }

});
